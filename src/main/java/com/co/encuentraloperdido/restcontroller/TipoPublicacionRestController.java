package com.co.encuentraloperdido.restcontroller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.co.encuentraloperdido.modelo.TipoPublicacionModelo;

import com.co.encuentraloperdido.services.TipoPublicacionService;
;

@RestController
@RequestMapping("/api")
@CrossOrigin("http://localhost:4200")

public class TipoPublicacionRestController {
	
	
	
		
		@Autowired
		private TipoPublicacionService tipoPublicacionService;
		
		@GetMapping("/listar/tipo")
		public List<TipoPublicacionModelo> listAll(){
			return tipoPublicacionService.listAll();		
		}
		@GetMapping("/tipo/{id}")
		public TipoPublicacionModelo findById(@PathVariable long id) {
			return tipoPublicacionService.findById(id);
		}
		@PostMapping("/insertar/tipo")
		public ResponseEntity<?> insert(@RequestBody TipoPublicacionModelo tipo) {
			Map<String,Object> response = new HashMap<>();
			HttpStatus status;
			try {
				tipoPublicacionService.save(tipo);
				response.put("Mensaje", "La información se ha insertado exitosamente");
				status = HttpStatus.CREATED;
			} catch (BindException e) {
				response.put("Causa", e.getObjectName().toString());
				response.put("Mensaje", "No se pudo insertar la información");
				status = HttpStatus.BAD_REQUEST;
			}
			return new ResponseEntity<>(response, status);
		}
			
		@DeleteMapping("eliminar/tipo/{id}")
		@ResponseStatus(HttpStatus.ACCEPTED)
		public void delete(@PathVariable long id) {
			tipoPublicacionService.deleteById(id);
		}
		
		
		@PutMapping("/actualizar/tipo")
		public ResponseEntity<?> actualizar(@RequestBody TipoPublicacionModelo tipo) {
			Map<String,Object> response = new HashMap<>();
			HttpStatus status;
			
			try {
				tipoPublicacionService.update(tipo);
				response.put("Mensaje", "La información se ha actualizado exitosamente");
				status = HttpStatus.CREATED;
			} catch (BindException e) {
				response.put("Causa", e.getObjectName().toString());
				response.put("Mensaje", "No se pudo insertar la información, la tipo no existe");
				status = HttpStatus.BAD_REQUEST;
			}
			return new ResponseEntity<>(response, status);
		}
		@PutMapping("/actualizar/tipo/{id}")
		public ResponseEntity<?> actualizarById(@RequestBody TipoPublicacionModelo tipo,@PathVariable long id) throws BindException {
			Map<String,Object> response = new HashMap<>();
			HttpStatus status;
			List<TipoPublicacionModelo> tipos = listAll();
			for(int i=0; i< tipos.size(); i++) {
		
				if(tipos.get(i).getId()==id)
				{
					tipoPublicacionService.updateById(tipo, id);
					response.put("Mensaje", "La información se ha actualizado exitosamente");
					status = HttpStatus.CREATED;
					return new ResponseEntity<>(response, status);
				}
				
			}
				response.put("Mensaje", "No se pudo insertar la información, la categotia  no existe");
				status = HttpStatus.BAD_REQUEST;
				return new ResponseEntity<>(response, status);
		}


}
