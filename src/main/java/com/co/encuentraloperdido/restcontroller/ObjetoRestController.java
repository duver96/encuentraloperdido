package com.co.encuentraloperdido.restcontroller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.co.encuentraloperdido.modelo.ObjetoModelo;
import com.co.encuentraloperdido.services.ObjetoService;

@RestController
@RequestMapping("/api")
@CrossOrigin("http://localhost:4200")
public class ObjetoRestController {
	
	@Autowired
	private ObjetoService objetoService;
	@GetMapping("/listar/objeto")
	public List<ObjetoModelo> listAll(){
		return objetoService.listAll();		
	}
	@GetMapping("/objeto/{id}")
	public ObjetoModelo findById(@PathVariable long id) {
		return objetoService.findById(id);
	}
	@PostMapping("/insertar/objeto")
	public ResponseEntity<?> insert(@RequestBody ObjetoModelo objeto) {
		Map<String,Object> response = new HashMap<>();
		HttpStatus status;
		try {
			objetoService.save(objeto);
			response.put("Mensaje", "La información se ha insertado exitosamente");
			status = HttpStatus.CREATED;
		} catch (BindException e) {
			response.put("Causa", e.getObjectName().toString());
			response.put("Mensaje", "No se pudo insertar la información");
			status = HttpStatus.BAD_REQUEST;
		}
		return new ResponseEntity<>(response, status);
	}
		
	@DeleteMapping("eliminar/objeto/{id}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void delete(@PathVariable long id) {
		objetoService.deleteById(id);
	}
	
	
	@PutMapping("/actualizar/objeto")
	public ResponseEntity<?> actualizar(@RequestBody ObjetoModelo objeto) {
		Map<String,Object> response = new HashMap<>();
		HttpStatus status;
		
		try {
			objetoService.update(objeto);
			response.put("Mensaje", "La información se ha actualizado exitosamente");
			status = HttpStatus.CREATED;
		} catch (BindException e) {
			response.put("Causa", e.getObjectName().toString());
			response.put("Mensaje", "No se pudo insertar la información, la objeto no existe");
			status = HttpStatus.BAD_REQUEST;
		}
		return new ResponseEntity<>(response, status);
	}
	@PutMapping("/actualizar/objeto/{id}")
	public ResponseEntity<?> actualizarById(@RequestBody ObjetoModelo objeto,@PathVariable long id) throws BindException {
		Map<String,Object> response = new HashMap<>();
		HttpStatus status;
		List<ObjetoModelo> objetos = listAll();
		for(int i=0; i< objetos.size(); i++) {
	
			if(objetos.get(i).getId()==id)
			{
				objetoService.updateById(objeto, id);
				response.put("Mensaje", "La información se ha actualizado exitosamente");
				status = HttpStatus.CREATED;
				return new ResponseEntity<>(response, status);
			}
			
		}
			response.put("Mensaje", "No se pudo insertar la información, la objeto no existe");
			status = HttpStatus.BAD_REQUEST;
			return new ResponseEntity<>(response, status);
	}


}
