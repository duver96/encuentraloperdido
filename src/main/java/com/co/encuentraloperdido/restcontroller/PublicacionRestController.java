package com.co.encuentraloperdido.restcontroller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.co.encuentraloperdido.modelo.PublicacionModelo;

import com.co.encuentraloperdido.services.PublicacionService;


@RestController
@RequestMapping("/api")
@CrossOrigin("http://localhost:4200")
public class PublicacionRestController {
	
	@Autowired
	private PublicacionService publicacionService;

	
	@GetMapping("/listar/publicacion")
	public List<PublicacionModelo> listAll(){
		return publicacionService.listAll();		
	}
	@GetMapping("/publicacion/{id}")
	public PublicacionModelo findById(@PathVariable long id) {
		return publicacionService.findById(id);
	}
	@PostMapping("/insertar/publicacion")
	public ResponseEntity<?> insert(@RequestBody PublicacionModelo publicacion) {
		Map<String,Object> response = new HashMap<>();
		HttpStatus status;
		try {
			
			publicacionService.save(publicacion);
			
			
			response.put("Mensaje", "La información se ha insertado exitosamente");
			status = HttpStatus.CREATED;
		} catch (BindException e) {
			response.put("Causa", e.getObjectName().toString());
			response.put("Mensaje", "No se pudo insertar la información");
			status = HttpStatus.BAD_REQUEST;
		}
		return new ResponseEntity<>(response, status);
	}
		
	@DeleteMapping("eliminar/publicacion/{id}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void delete(@PathVariable long id) {
		publicacionService.deleteById(id);
	}
	
	
	@PutMapping("/actualizar/publicacion")
	public ResponseEntity<?> actualizar(@RequestBody PublicacionModelo publicacion) {
		Map<String,Object> response = new HashMap<>();
		HttpStatus status;
		
		try {
			publicacionService.update(publicacion);
			response.put("Mensaje", "La información se ha actualizado exitosamente");
			status = HttpStatus.CREATED;
		} catch (BindException e) {
			response.put("Causa", e.getObjectName().toString());
			response.put("Mensaje", "No se pudo insertar la información, la persona no existe");
			status = HttpStatus.BAD_REQUEST;
		}
		return new ResponseEntity<>(response, status);
	}
	@PutMapping("/actualizar/publicacion/{id}")
	public ResponseEntity<?> actualizarById(@RequestBody PublicacionModelo publicacion,@PathVariable long id) throws BindException {
		Map<String,Object> response = new HashMap<>();
		HttpStatus status;
		List<PublicacionModelo> publicaciones = listAll();
		for(int i=0; i< publicaciones.size(); i++) {
	
			if(publicaciones.get(i).getId()==id)
			{
				publicacionService.updateById(publicacion, id);
				response.put("Mensaje", "La información se ha actualizado exitosamente");
				status = HttpStatus.CREATED;
				return new ResponseEntity<>(response, status);
			}
			
		}
			response.put("Mensaje", "No se pudo insertar la información, la persona no existe");
			status = HttpStatus.BAD_REQUEST;
			return new ResponseEntity<>(response, status);
	}

}
