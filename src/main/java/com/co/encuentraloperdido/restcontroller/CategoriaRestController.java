package com.co.encuentraloperdido.restcontroller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.co.encuentraloperdido.modelo.CategoriaModelo;

import com.co.encuentraloperdido.services.CategoriaService;

@RestController
@RequestMapping("/api")
@CrossOrigin("http://localhost:4200")
public class CategoriaRestController {
	
	@Autowired
	private CategoriaService categoriaService;
	
	@GetMapping("/listar/categoria")
	public List<CategoriaModelo> listAll(){
		return categoriaService.listAll();		
	}
	@GetMapping("/categoria/{id}")
	public CategoriaModelo findById(@PathVariable long id) {
		return categoriaService.findById(id);
	}
	@PostMapping("/insertar/categoria")
	public ResponseEntity<?> insert(@RequestBody CategoriaModelo categoria) {
		Map<String,Object> response = new HashMap<>();
		HttpStatus status;
		try {
			categoriaService.save(categoria);
			response.put("Mensaje", "La información se ha insertado exitosamente");
			status = HttpStatus.CREATED;
		} catch (BindException e) {
			response.put("Causa", e.getObjectName().toString());
			response.put("Mensaje", "No se pudo insertar la información");
			status = HttpStatus.BAD_REQUEST;
		}
		return new ResponseEntity<>(response, status);
	}
		
	@DeleteMapping("eliminar/categoria/{id}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void delete(@PathVariable long id) {
		categoriaService.deleteById(id);
	}
	
	
	@PutMapping("/actualizar/categoria")
	public ResponseEntity<?> actualizar(@RequestBody CategoriaModelo categoria) {
		Map<String,Object> response = new HashMap<>();
		HttpStatus status;
		
		try {
			categoriaService.update(categoria);
			response.put("Mensaje", "La información se ha actualizado exitosamente");
			status = HttpStatus.CREATED;
		} catch (BindException e) {
			response.put("Causa", e.getObjectName().toString());
			response.put("Mensaje", "No se pudo insertar la información, la categoria no existe");
			status = HttpStatus.BAD_REQUEST;
		}
		return new ResponseEntity<>(response, status);
	}
	@PutMapping("/actualizar/categoria/{id}")
	public ResponseEntity<?> actualizarById(@RequestBody CategoriaModelo categoria,@PathVariable long id) throws BindException {
		Map<String,Object> response = new HashMap<>();
		HttpStatus status;
		List<CategoriaModelo> categorias = listAll();
		for(int i=0; i< categorias.size(); i++) {
	
			if(categorias.get(i).getId()==id)
			{
				categoriaService.updateById(categoria, id);
				response.put("Mensaje", "La información se ha actualizado exitosamente");
				status = HttpStatus.CREATED;
				return new ResponseEntity<>(response, status);
			}
			
		}
			response.put("Mensaje", "No se pudo insertar la información, la categotia  no existe");
			status = HttpStatus.BAD_REQUEST;
			return new ResponseEntity<>(response, status);
	}

	

}
