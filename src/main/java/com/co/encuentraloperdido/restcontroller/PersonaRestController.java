package com.co.encuentraloperdido.restcontroller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.co.encuentraloperdido.modelo.PersonaModelo;
import com.co.encuentraloperdido.services.PersonaService;



@RestController
@RequestMapping("/api")
@CrossOrigin("http://localhost:4200")
public class PersonaRestController {
	
		
	@Autowired
	private PersonaService personaService;
	@GetMapping("/listar/persona")
	public List<PersonaModelo> listAll(){
		return personaService.listAll();		
	}
	@GetMapping("/persona/{id}")
	public PersonaModelo findById(@PathVariable long id) {
		return personaService.findById(id);
	}
	@PostMapping("/insertar/persona")
	public ResponseEntity<?> insert(@RequestBody PersonaModelo persona) {
		Map<String,Object> response = new HashMap<>();
		HttpStatus status;
		try {
			personaService.save(persona);
			response.put("Mensaje", "La información se ha insertado exitosamente");
			status = HttpStatus.CREATED;
		} catch (BindException e) {
			response.put("Causa", e.getObjectName().toString());
			response.put("Mensaje", "No se pudo insertar la información");
			status = HttpStatus.BAD_REQUEST;
		}
		return new ResponseEntity<>(response, status);
	}
		
	@DeleteMapping("eliminar/persona/{id}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void delete(@PathVariable long id) {
		personaService.deleteById(id);
	}
	
	
	@PutMapping("/actualizar/persona")
	public ResponseEntity<?> actualizar(@RequestBody PersonaModelo persona) {
		Map<String,Object> response = new HashMap<>();
		HttpStatus status;
		
		try {
			personaService.update(persona);
			response.put("Mensaje", "La información se ha actualizado exitosamente");
			status = HttpStatus.CREATED;
		} catch (BindException e) {
			response.put("Causa", e.getObjectName().toString());
			response.put("Mensaje", "No se pudo insertar la información, la persona no existe");
			status = HttpStatus.BAD_REQUEST;
		}
		return new ResponseEntity<>(response, status);
	}
	@PutMapping("/actualizar/persona/{id}")
	public ResponseEntity<?> actualizarById(@RequestBody PersonaModelo persona,@PathVariable long id) throws BindException {
		Map<String,Object> response = new HashMap<>();
		HttpStatus status;
		List<PersonaModelo> personas = listAll();
		for(int i=0; i< personas.size(); i++) {
	
			if(personas.get(i).getId()==id)
			{
				personaService.updateById(persona, id);
				response.put("Mensaje", "La información se ha actualizado exitosamente");
				status = HttpStatus.CREATED;
				return new ResponseEntity<>(response, status);
			}
			
		}
			response.put("Mensaje", "No se pudo insertar la información, la persona no existe");
			status = HttpStatus.BAD_REQUEST;
			return new ResponseEntity<>(response, status);
	}

}
