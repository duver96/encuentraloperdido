package com.co.encuentraloperdido.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.co.encuentraloperdido.entity.CategoriaEntity;

public interface CategoriaRepository extends JpaRepository<CategoriaEntity, Serializable > {

}
