package com.co.encuentraloperdido.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.co.encuentraloperdido.entity.TipoPublicacionEntity;

public interface TipoPublicacionRepository extends JpaRepository<TipoPublicacionEntity, Serializable> {

}
