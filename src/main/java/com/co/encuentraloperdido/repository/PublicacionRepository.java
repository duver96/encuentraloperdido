package com.co.encuentraloperdido.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.co.encuentraloperdido.entity.PublicacionEntity;

public interface PublicacionRepository extends JpaRepository<PublicacionEntity, Serializable> {

}
