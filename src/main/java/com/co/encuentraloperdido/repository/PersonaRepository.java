package com.co.encuentraloperdido.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.co.encuentraloperdido.entity.PersonaEntity;

public interface PersonaRepository extends JpaRepository<PersonaEntity, Serializable> {

}
