package com.co.encuentraloperdido.services;

import java.util.List;

import org.springframework.validation.BindException;

import com.co.encuentraloperdido.modelo.CategoriaModelo;


public interface CategoriaService {
	
	List<CategoriaModelo> listAll();
	
	void save(CategoriaModelo categoria) throws BindException;
	
	CategoriaModelo findById(Long id);
	void deleteById(Long id);
	void update(CategoriaModelo categoria) throws BindException;
	void updateById(CategoriaModelo categoria, long id) throws BindException;

}
