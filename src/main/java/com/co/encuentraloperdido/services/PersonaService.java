package com.co.encuentraloperdido.services;

import java.util.List;

import org.springframework.validation.BindException;

import com.co.encuentraloperdido.modelo.PersonaModelo;



public interface PersonaService {
	
List<PersonaModelo> listAll();
	
	void save(PersonaModelo persona) throws BindException;
	
	PersonaModelo findById(Long id);
	void deleteById(Long id);
	void update(PersonaModelo persona) throws BindException;
	void updateById(PersonaModelo persona, long id) throws BindException;
	 

}
