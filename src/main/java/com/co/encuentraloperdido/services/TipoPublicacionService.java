package com.co.encuentraloperdido.services;

import java.util.List;

import org.springframework.validation.BindException;


import com.co.encuentraloperdido.modelo.TipoPublicacionModelo;

public interface TipoPublicacionService {
	
	
	List<TipoPublicacionModelo> listAll();
	
	void save(TipoPublicacionModelo tipoPublicacion) throws BindException;
	
	TipoPublicacionModelo  findById(Long id);
	void deleteById(Long id);
	void update(TipoPublicacionModelo tipoPublicacion) throws BindException;
	void updateById(TipoPublicacionModelo tipoPublicacion, long id) throws BindException;

}
