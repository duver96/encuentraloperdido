package com.co.encuentraloperdido.services;

import java.util.List;

import org.springframework.validation.BindException;

import com.co.encuentraloperdido.modelo.PublicacionModelo;



public interface PublicacionService {
	
	List<PublicacionModelo> listAll();
	
	void save(PublicacionModelo persona) throws BindException;
	
	PublicacionModelo findById(Long id);
	void deleteById(Long id);
	void update(PublicacionModelo publicacion) throws BindException;
	void updateById(PublicacionModelo publicacion, long id) throws BindException;

}
