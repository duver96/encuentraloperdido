package com.co.encuentraloperdido.services;

import java.util.List;

import org.springframework.validation.BindException;

import com.co.encuentraloperdido.modelo.ObjetoModelo;

public interface ObjetoService {
	
	List<ObjetoModelo> listAll();
	
	void save(ObjetoModelo objeto) throws BindException;
	
	ObjetoModelo findById(Long id);
	void deleteById(Long id);
	void update(ObjetoModelo objeto) throws BindException;
	void updateById(ObjetoModelo objeto, long id) throws BindException;

}
