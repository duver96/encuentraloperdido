package com.co.encuentraloperdido.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;

import com.co.encuentraloperdido.converter.CategoriaConverter;
import com.co.encuentraloperdido.modelo.CategoriaModelo;
import com.co.encuentraloperdido.repository.CategoriaRepository;
import com.co.encuentraloperdido.services.CategoriaService;
@Service
public class CategoriaServiceImpl implements CategoriaService{

	@Autowired
	private CategoriaConverter converter;
	
	@Autowired
	private CategoriaRepository repository;
	
	@Override
	public List<CategoriaModelo> listAll() {
		
		return converter.entityToModel(repository.findAll());
	}
	@Override
	public void save(CategoriaModelo categoria) throws BindException {
		repository.save(converter.modelToEntity(categoria));
		
	}

	@Override
	public CategoriaModelo findById(Long id) {
		
		return converter.entityToModel(repository.findById(id).orElse(null));
	}

	@Override
	public void deleteById(Long id) {
		
		repository.deleteById(id);
	}

	@Override
	public void update(CategoriaModelo categoria) throws BindException {
		
		repository.save(converter.modelToEntity(categoria));
		
	}

	@Override
	public void updateById(CategoriaModelo categoria, long id) throws BindException {
		
		categoria.setId(id);
		repository.save(converter.modelToEntity(categoria));
		
	}

	
	
	


}
