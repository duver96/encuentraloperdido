package com.co.encuentraloperdido.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;

import com.co.encuentraloperdido.converter.TipoPublicacionConverter;
import com.co.encuentraloperdido.modelo.TipoPublicacionModelo;
import com.co.encuentraloperdido.repository.TipoPublicacionRepository;
import com.co.encuentraloperdido.services.TipoPublicacionService;

@Service
public class TipoPublicacionServiceImpl implements TipoPublicacionService{

	@Autowired
	TipoPublicacionRepository tipoPublicacionRepository;
	
	@Autowired
	TipoPublicacionConverter converter;
	@Override
	public List<TipoPublicacionModelo> listAll() {
		
		return converter.entityToModel(tipoPublicacionRepository.findAll());
		
	}

	@Override
	public void save(TipoPublicacionModelo tipoPublicacion) throws BindException {
		
		
	}

	@Override
	public TipoPublicacionModelo findById(Long id) {
		
		return null;
	}

	@Override
	public void deleteById(Long id) {
		
	}

	@Override
	public void update(TipoPublicacionModelo tipoPublicacion) throws BindException {
		
		
	}

	@Override
	public void updateById(TipoPublicacionModelo tipoPublicacion, long id) throws BindException {
		// TODO Auto-generated method stub
		
	}

}
