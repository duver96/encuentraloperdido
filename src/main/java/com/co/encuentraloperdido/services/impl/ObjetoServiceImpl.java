package com.co.encuentraloperdido.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;

import com.co.encuentraloperdido.converter.ObjetoConverter;
import com.co.encuentraloperdido.modelo.ObjetoModelo;
import com.co.encuentraloperdido.repository.ObjetoRepository;
import com.co.encuentraloperdido.services.ObjetoService;
@Service
public class ObjetoServiceImpl implements ObjetoService{

	@Autowired
	private ObjetoConverter converter;
	@Autowired
	private ObjetoRepository repository;
	
	@Override
	public List<ObjetoModelo> listAll() {
		
		return converter.entityToModel(repository.findAll());
	}

	@Override
	public void save(ObjetoModelo objeto) throws BindException {
		
		repository.save(converter.modelToEntity(objeto));
		
	}

	@Override
	public ObjetoModelo findById(Long id) {
		
		return converter.entityToModel(repository.findById(id).orElse(null));
	}

	@Override
	public void deleteById(Long id) {
		
		repository.deleteById(id);
		
	}

	@Override
	public void update(ObjetoModelo objeto) throws BindException {
		
		repository.save(converter.modelToEntity(objeto));
		
	}

	@Override
	public void updateById(ObjetoModelo objeto, long id) throws BindException {
		objeto.setId(id);
		repository.save(converter.modelToEntity(objeto));
		
	}

}
