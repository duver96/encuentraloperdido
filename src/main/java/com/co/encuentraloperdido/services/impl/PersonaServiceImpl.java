package com.co.encuentraloperdido.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;

import com.co.encuentraloperdido.converter.PersonaConverter;
import com.co.encuentraloperdido.modelo.PersonaModelo;
import com.co.encuentraloperdido.repository.PersonaRepository;
import com.co.encuentraloperdido.services.PersonaService;



@Service
public class PersonaServiceImpl implements PersonaService{
	@Autowired
	private PersonaRepository personaRepository;
	@Autowired
	private PersonaConverter converter;

	@Override
	public List<PersonaModelo> listAll() {
		return converter.entityToModel(personaRepository.findAll());
	}

	@Override
	public void save(PersonaModelo persona) throws BindException {
		personaRepository.save(converter.modelToEntity(persona));
	}

	@Override
	public PersonaModelo findById(Long id) {
		return converter.entityToModel(personaRepository.findById(id).orElse(null));
	}

	@Override
	public void deleteById(Long id) {
		 personaRepository.deleteById(id);
	}

	@Override
	public void update(PersonaModelo persona) throws BindException {
			personaRepository.save(converter.modelToEntity(persona));
		
	}

	@Override
	public void updateById(PersonaModelo persona,long id) throws BindException {
		persona.setId(id);
		personaRepository.save(converter.modelToEntity(persona));
		
	}

	

}
