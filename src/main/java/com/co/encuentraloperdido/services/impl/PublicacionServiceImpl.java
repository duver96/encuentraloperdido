package com.co.encuentraloperdido.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;


import com.co.encuentraloperdido.converter.PublicacionConverter;
import com.co.encuentraloperdido.modelo.PublicacionModelo;

import com.co.encuentraloperdido.repository.PublicacionRepository;
import com.co.encuentraloperdido.services.PublicacionService;
@Service
public class PublicacionServiceImpl implements PublicacionService{

	@Autowired
	private PublicacionConverter converter;
	
	@Autowired
	private PublicacionRepository publicacionRepository;
	@Override
	public List<PublicacionModelo> listAll() {
		return converter.entityToModel(publicacionRepository.findAll());
	}

	@Override
	public void save(PublicacionModelo publicacion) throws BindException {
		
		
		publicacionRepository.save(converter.modelToEntity(publicacion));
	}

	@Override
	public PublicacionModelo findById(Long id) {
		
		return converter.entityToModel(publicacionRepository.findById(id).orElse(null));
	}

	@Override
	public void deleteById(Long id) {
		publicacionRepository.deleteById(id);
		
	}

	@Override
	public void update(PublicacionModelo publicacion) throws BindException {
		publicacionRepository.save(converter.modelToEntity(publicacion));
	}

	@Override
	public void updateById(PublicacionModelo publicacion, long id) throws BindException {
				
		publicacion.setId(id);
		publicacionRepository.save(converter.modelToEntity(publicacion));
		
	}

}
