package com.co.encuentraloperdido.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;

import com.co.encuentraloperdido.entity.ObjetoEntity;

import com.co.encuentraloperdido.modelo.ObjetoModelo;
import com.co.encuentraloperdido.util.validator.ObjetoValidator;



@Component
public class ObjetoConverter {

	@Autowired
	private ObjetoValidator objetoValidator;
	@Autowired PublicacionConverter publicacionConverter;
	
	public ObjetoModelo entityToModel(ObjetoEntity objetoEntity)
	{
		ObjetoModelo objeto = new ObjetoModelo();
		objeto.setId(objetoEntity.getId());
		objeto.setDescripcion(objetoEntity.getDescripcion());
		objeto.setFecha(objetoEntity.getFecha());
		objeto.setLugar(objetoEntity.getLugar());
		
			
		return objeto;
	
	}
	
	public ObjetoEntity modelToEntity(ObjetoModelo objeto) throws BindException
	{
		ObjetoEntity objetoEntity = new ObjetoEntity();
		objetoEntity.setId(objeto.getId());
		objetoEntity.setDescripcion(objeto.getDescripcion());
		objetoEntity.setFecha(objeto.getFecha());
		objetoEntity.setLugar(objeto.getLugar());
		
		objetoValidator.validate(objetoEntity);		
		return objetoEntity;
			
	}
	public List<ObjetoModelo> entityToModel(List<ObjetoEntity> objetoEntity) {
		List<ObjetoModelo> objetos = new ArrayList<ObjetoModelo>(objetoEntity.size());
		objetoEntity.forEach((entity) -> {
			objetos.add(entityToModel(entity));
		});
		return objetos;
	}
	
	public List<ObjetoEntity> modelToEntity(List<ObjetoModelo> objeto) {
		List<ObjetoEntity> objetoEntity = new ArrayList<ObjetoEntity>(objeto.size());
		objeto.forEach((entity) -> {
			try {
				objetoEntity.add(modelToEntity(entity));
			} catch (BindException e) {
				
				e.printStackTrace();
			}
		});
		return objetoEntity;
	}

	
	
	
	
}
