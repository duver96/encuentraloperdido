package com.co.encuentraloperdido.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;


import com.co.encuentraloperdido.entity.TipoPublicacionEntity;

import com.co.encuentraloperdido.modelo.TipoPublicacionModelo;

@Component
public class TipoPublicacionConverter {
	
	public TipoPublicacionModelo entityToModel(TipoPublicacionEntity tipoPublicacionEntity) {
		TipoPublicacionModelo tipoPublicacion = new TipoPublicacionModelo();
		tipoPublicacion.setId(tipoPublicacionEntity.getId());
		tipoPublicacion.setTipoPublicacion(tipoPublicacionEntity.getTipoPublicacion());
		
		return tipoPublicacion;
	}
	
	public TipoPublicacionEntity modelToEntity(TipoPublicacionModelo tipoPublicacionModelo) {
		TipoPublicacionEntity tipoPublicacionEntity = new TipoPublicacionEntity();
		tipoPublicacionEntity.setId(tipoPublicacionModelo.getId());
		tipoPublicacionEntity.setTipoPublicacion(tipoPublicacionModelo.getTipoPublicacion());
		
		return tipoPublicacionEntity;
	
	
	}
	
	public List<TipoPublicacionModelo> entityToModel(List<TipoPublicacionEntity> tipoPublicacionEntity) {
		List<TipoPublicacionModelo> tipoPublicacion= new ArrayList<TipoPublicacionModelo>(tipoPublicacionEntity.size());
		tipoPublicacionEntity.forEach((entity) -> {
			tipoPublicacion.add(entityToModel(entity));
		});
		return tipoPublicacion;
	}
	
	

}
