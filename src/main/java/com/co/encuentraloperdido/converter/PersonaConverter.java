package com.co.encuentraloperdido.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;

import com.co.encuentraloperdido.entity.PersonaEntity;
import com.co.encuentraloperdido.modelo.PersonaModelo;
import com.co.encuentraloperdido.util.validator.PersonaValidator;




@Component
public class PersonaConverter {
	
	@Autowired
	private PersonaValidator personaValidator;
	
	public PersonaModelo entityToModel(PersonaEntity personaEntity) {
		PersonaModelo persona=new PersonaModelo();
		persona.setId(personaEntity.getId());
		persona.setNombre(personaEntity.getNombre());
		persona.setApellido(personaEntity.getApellido());
		persona.setTelefono(personaEntity.getTelefono());
		persona.setDocumento(personaEntity.getDocumento());
	
		return persona;
		
	}
	public PersonaEntity modelToEntity(PersonaModelo persona) throws BindException {
		PersonaEntity personaEntity = new PersonaEntity();
		personaEntity.setId(persona.getId());
		personaEntity.setNombre(persona.getNombre());
		personaEntity.setApellido(persona.getApellido());
		personaEntity.setTelefono(persona.getTelefono());
		personaEntity.setDocumento(persona.getDocumento());
		personaValidator.validate(personaEntity);
		return personaEntity;
	}

	
	public List<PersonaModelo> entityToModel(List<PersonaEntity> personasEntity) {
		List<PersonaModelo> personas = new ArrayList<PersonaModelo>(personasEntity.size());
		personasEntity.forEach((entity) -> {
			personas.add(entityToModel(entity));
		});
		return personas;
	}
	
	}


