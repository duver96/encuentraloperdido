package com.co.encuentraloperdido.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;

import com.co.encuentraloperdido.entity.CategoriaEntity;
import com.co.encuentraloperdido.modelo.CategoriaModelo;
import com.co.encuentraloperdido.util.validator.CategoriaValidator;

@Component
public class CategoriaConverter {
	
	@Autowired
	private CategoriaValidator validator;
	
	public CategoriaEntity modelToEntity(CategoriaModelo categoria) throws BindException {
		
		CategoriaEntity categoriaEntity = new CategoriaEntity();
		
		categoriaEntity.setId(categoria.getId());
		categoriaEntity.setCategoria(categoria.getCategoria());
		validator.validate(categoriaEntity);

		return categoriaEntity;
	}
	public CategoriaModelo entityToModel(CategoriaEntity categoriaEntity)
	{
		CategoriaModelo categoria = new CategoriaModelo();
		categoria.setId(categoriaEntity.getId());
		categoria.setCategoria(categoriaEntity.getCategoria());
		
		return categoria;
	}
	
	public List<CategoriaModelo> entityToModel(List<CategoriaEntity> categoriaEntity) {
		List<CategoriaModelo> categorias = new ArrayList<CategoriaModelo>(categoriaEntity.size());
		categoriaEntity.forEach((entity) -> {
			categorias.add(entityToModel(entity));
		});
		return categorias;
	}

}
