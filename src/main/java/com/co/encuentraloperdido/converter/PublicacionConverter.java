package com.co.encuentraloperdido.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;


import com.co.encuentraloperdido.entity.PublicacionEntity;

import com.co.encuentraloperdido.modelo.PublicacionModelo;
import com.co.encuentraloperdido.util.validator.PublicacionValidator;


@Component
public class PublicacionConverter {
	@Autowired
	private PublicacionValidator publicacionValidator;
	@Autowired
	private PersonaConverter personaConverter;
	@Autowired
	private CategoriaConverter categoriaConverter;
	@Autowired
	private ObjetoConverter objetoConverter;
	@Autowired
	private TipoPublicacionConverter tipoPublicacionConverter;
	
	public PublicacionModelo entityToModel(PublicacionEntity publicacionEntity) {
		PublicacionModelo publicacion=new PublicacionModelo();
		publicacion.setId(publicacionEntity.getId());
		
		publicacion.setFecha(publicacionEntity.getFecha());
		publicacion.setDescripcion(publicacionEntity.getDescripcion());
		
		publicacion.setPersona(personaConverter.entityToModel(publicacionEntity.getPersona()));
		
		publicacion.setObjetos(objetoConverter.entityToModel(publicacionEntity.getObjetos()));
		
		publicacion.setCategorias(categoriaConverter.entityToModel(publicacionEntity.getCategorias()));
		
		
		publicacion.setTipoPublicacion(tipoPublicacionConverter.entityToModel(publicacionEntity.getTipoPublicación()));
		
	
		return publicacion;
	}
		
	public PublicacionEntity modelToEntity(PublicacionModelo publicacion) throws BindException {
		PublicacionEntity publicacionEntity = new PublicacionEntity();
				
		publicacionEntity.setId(publicacion.getId());
		publicacionEntity.setFecha(publicacion.getFecha());
		publicacionEntity.setDescripcion(publicacion.getDescripcion());
		
		publicacionEntity.setPersona(personaConverter.modelToEntity(publicacion.getPersona()));
				
		publicacionEntity.setCategorias(categoriaConverter.modelToEntity(publicacion.getCategorias()));
		
		publicacionEntity.setObjetos(objetoConverter.modelToEntity(publicacion.getObjetos()));
		
			
		publicacionEntity.setTipoPublicación(tipoPublicacionConverter.modelToEntity(publicacion.getTipoPublicacion()));
		
		
		
		
		publicacionValidator.validate(publicacionEntity);
		return publicacionEntity;
	}

	
	public List<PublicacionModelo> entityToModel(List<PublicacionEntity> publicacionEntity) {
		List<PublicacionModelo> publicacion= new ArrayList<PublicacionModelo>(publicacionEntity.size());
		publicacionEntity.forEach((entity) -> {
			publicacion.add(entityToModel(entity));
		});
		return publicacion;
	}
	
	}


