package com.co.encuentraloperdido.entity;



import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;

import javax.persistence.ManyToOne;
import javax.persistence.Table;


import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Table(name="publicacion")
@NoArgsConstructor
public class PublicacionEntity {
	
	@Id
	@Column(name="id")
	@GeneratedValue
	private long id;
	@Column(name="fecha",nullable=false, length=50)
	private String fecha;
	
	@Column(name="descripcion",  nullable=false, length=200)
	private String descripcion;
	
	
	@ManyToOne(cascade = {CascadeType.PERSIST})
	@JoinColumn(name = "id_persona", nullable = true)
	private PersonaEntity persona;
	

	@ManyToOne(cascade = CascadeType.PERSIST)	
	@JoinColumn(name="id_objeto")
	private ObjetoEntity objetos;
	
	@ManyToOne(cascade = {CascadeType.PERSIST})
	@JoinColumn(name="id_tipoPublicacion", nullable=true)
	private TipoPublicacionEntity tipoPublicación;
	

	
	@ManyToOne(cascade = {CascadeType.PERSIST})
	@JoinTable(name = "id_categoria")
	private CategoriaEntity categorias;
	
	
}
