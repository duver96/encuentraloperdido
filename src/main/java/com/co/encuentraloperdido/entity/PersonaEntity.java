package com.co.encuentraloperdido.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import javax.persistence.Table;


import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="Persona")
@Data
@NoArgsConstructor
public class PersonaEntity {
	@Id
	@Column(name="id")
	@GeneratedValue
	private long id;
	@Column(name="nombre",  nullable=false, length=50)
	private String nombre;
	@Column(name="apellido", nullable=false, length=50)
	private String apellido;
	@Column(name="telefono", nullable=false, length=30)
	private String telefono;
	
	@Column(name = "documento", nullable=false, length=50)
	private String documento;
	

	
	
	

}
