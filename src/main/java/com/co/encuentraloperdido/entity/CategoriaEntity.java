package com.co.encuentraloperdido.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Table(name="categoria")
@NoArgsConstructor
public class CategoriaEntity {
	
	@Id
	@Column(name="id")
	@GeneratedValue
	private long id;
	@Column(name="categoria",  nullable=false, length=50)
	private String categoria;

}
