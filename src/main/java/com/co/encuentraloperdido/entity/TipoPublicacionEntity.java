package com.co.encuentraloperdido.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Table(name="tipoPublicacion")
@NoArgsConstructor
public class TipoPublicacionEntity {
	
	@Id
	@Column(name="id")
	@GeneratedValue
	private long id;
	@Column(name="tipoPublicacion", nullable=false, length=50)
	private String tipoPublicacion;


}
