package com.co.encuentraloperdido.entity;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


import lombok.Data;
import lombok.NoArgsConstructor;
@Entity
@Data
@Table(name="objeto")
@NoArgsConstructor
public class ObjetoEntity {
	@Id
	@Column(name="id")
	@GeneratedValue
	private long id;
	
	@Column(name="descripcion", nullable=false, length=200)
	private String descripcion;
	
	@Column(name="lugar", nullable=false, length=100)
	private String lugar;
	
	@Column(name="fecha",  nullable=false, length=50)
	private String fecha;
	
	//@Column(name="imagen")
	//private Image imagen;
	
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="id_publicacion")
	private PublicacionEntity publicacion;


}
