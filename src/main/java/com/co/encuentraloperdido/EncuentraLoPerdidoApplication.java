package com.co.encuentraloperdido;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

@SpringBootApplication
@Component
public class EncuentraLoPerdidoApplication {

	public static void main(String[] args) {
		SpringApplication.run(EncuentraLoPerdidoApplication.class, args);
	}

}
