package com.co.encuentraloperdido.util.validator;

import org.springframework.stereotype.Component;

import com.co.encuentraloperdido.entity.CategoriaEntity;
import com.co.encuentraloperdido.util.validator.generic.GenericValidator;

@Component
public class CategoriaValidator extends GenericValidator<CategoriaEntity> {
	
	public CategoriaValidator(){
		super();
	}

}
