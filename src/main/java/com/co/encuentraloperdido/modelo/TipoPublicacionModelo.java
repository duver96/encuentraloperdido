/*
 * El tipo de publicacion define si se trata de objetos perdidos o encontrados.
 */
package com.co.encuentraloperdido.modelo;

import lombok.Data;

@Data
public class TipoPublicacionModelo {
	
	private long id;
	private String tipoPublicacion;

}
