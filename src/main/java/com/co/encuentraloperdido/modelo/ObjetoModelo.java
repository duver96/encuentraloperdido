/*
 * Los objetos pueden haber sido perdidos o encontrados
 */
package com.co.encuentraloperdido.modelo;



import lombok.Data;

@Data
public class ObjetoModelo {
	private long id;
	private String descripcion;
	private String lugar;
	private String fecha;
	
//	private Image imagen;


}
