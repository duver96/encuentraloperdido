/*
 * La persona es el usuario de la persona que realiza publiciones de tipo "peridos" o "encontrados"
 */
package com.co.encuentraloperdido.modelo;



import lombok.Data;

@Data
public class PersonaModelo {
	private long id;
	private String nombre;
	private String apellido;
	private String telefono;
	private String documento;

	
}
