/*
 * Las publicaciones contienen objetos que fueron perdidos o encontrados.
 */

package com.co.encuentraloperdido.modelo;





import lombok.Data;

@Data
public class PublicacionModelo {
	private long id;
	private String fecha;
	private String descripcion;
	private ObjetoModelo objetos;
	private CategoriaModelo categorias;
	private PersonaModelo persona;
	private TipoPublicacionModelo tipoPublicacion;
	
	
	
	

}
